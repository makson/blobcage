#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This software is available to you under the terms of the GPL-3, see "/usr/share/common-licenses/GPL-3".
# Copyright:
# - Tomasz Makarewicz (tomasz.makarewicz@protonmail.ch)

class Bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    WHITE = '\033[1;37m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def info(message):
    print("[II] {}".format(message))


def success(message):
    print("{}[SS] {}{}".format(Bcolors.OKGREEN, message, Bcolors.ENDC))


def warning(message):
    print("{}[WW] {}{}".format(Bcolors.WARNING, message, Bcolors.ENDC))


def error(message):
    print("{}[EE] {}{}".format(Bcolors.FAIL, message, Bcolors.ENDC))


def command(message):
    print("{}[CC] {}{}".format(Bcolors.OKBLUE, message, Bcolors.ENDC))


def output(message):
    print("{}[OO] {}{}".format(Bcolors.WHITE, message, Bcolors.ENDC))
