#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This software is available to you under the terms of the GPL-3, see "/usr/share/common-licenses/GPL-3".
# Copyright:
# - Tomasz Makarewicz (tomasz.makarewicz@protonmail.ch)

from getpass import getpass
from subprocess import run, PIPE

from blobcage import messages


class Session:

    password = ""

    def __init__(self):
        if not self.password:
            self.password = getpass()

    def execute_sudo_cmd(self, command):

        sudo_cmd = "sudo -E" if "systemd-nspawn" not in command else "sudo"
        messages.command("{} {}".format(sudo_cmd, command))
        p = run("{} {}".format(sudo_cmd, command), stdout=PIPE, input=self.password, encoding='utf-8', shell=True)
        output = p.stdout
        exit_code = p.returncode
        messages.output(output)
        return output, exit_code
