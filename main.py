#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This software is available to you under the terms of the GPL-3, see "/usr/share/common-licenses/GPL-3".
# Copyright:
# - Tomasz Makarewicz (tomasz.makarewicz@protonmail.ch)

from subprocess import run, check_output

from blobcage import messages, session

import os
import shutil
import time


class Blobcage:

    version = "2.1"
    directory = "/srv/blobcage/"
    container_disto = "focal"
    distro = "linux"
    update_available = False
    menu_option = "X"
    err_msg = ""
    term = None

    def __init__(self):
        self.term = session.Session()

    def determine_distro(self):
        messages.info("Determine Linux distribution.")
        try:
            import apt
            self.distro = "debian"
            messages.success("Debian distribution identified.")
        except ImportError:
            messages.warning("Distribution not supported")
            self.distro = "linux"

    def install_dependencies(self):
        messages.info("Install Blobcage dependencies.")
        if self.distro in ["ubuntu", "debian"]:
            import apt
            requred_package_list = ["debootstrap", "systemd-container", "openssl", "x11-xserver-utils", "openssl",
                                    "mesa-utils", "python3"]
            cache = apt.cache.Cache()
            apt_install_required = False
            for package in requred_package_list:
                if not cache[package].is_installed:
                    apt_install_required = True
            if apt_install_required:
                cmd = "apt -y install"
                for required_package in requred_package_list:
                    cmd = "{} {}".format(cmd, required_package)
                self.term.execute_sudo_cmd(cmd)
                messages.success("Dependencies installed.")
            messages.success("Dependencies met.")
        else:
            self.err_msg = "Distribution {} is not supported".format(self.distro)

    def install_blobcage(self):
        messages.info("Install Blobcage.")
        self.term.execute_sudo_cmd("mkdir -p {}".format(self.directory))
        self.term.execute_sudo_cmd(
            "debootstrap --arch=amd64 {} {} http://archive.ubuntu.com/ubuntu/".format(self.container_disto,
                                                                                      self.directory))
        # Prepare repository list
        self.term.execute_sudo_cmd("rm {}etc/apt/sources.list".format(self.directory))
        self.term.execute_sudo_cmd("cp {}misc/sources.list {}/etc/apt/".format(blobcage_script_path, self.directory))
        self.term.execute_sudo_cmd(
            "cp {}misc/google-chrome.list {}/etc/apt/sources.list.d/".format(blobcage_script_path, self.directory))
        # Link Blobcage to local home dir
        link_target = "{}/blobcage".format(home_dir)
        if not os.path.exists(link_target) and not os.path.islink(link_target):
            os.symlink(self.directory, link_target)
        self.term.execute_sudo_cmd("mkdir {}blobcage-files/".format(self.directory))
        self.term.execute_sudo_cmd("chmod 777 {}blobcage-files".format(self.directory))
        scripts_list = os.listdir("{}scripts/".format(blobcage_script_path))
        for b_script in scripts_list:
            self.term.execute_sudo_cmd("cp {}scripts/{} {}/blobcage-files/".format(blobcage_script_path, b_script,
                                                                                   self.directory))
            self.term.execute_sudo_cmd("chmod 777 {}blobcage-files/{}".format(self.directory, b_script))
        # Prepare container from inside
        init_script = "/blobcage-files/init_script.sh"
        self.term.execute_sudo_cmd("systemd-nspawn -D {} {}".format(self.directory, init_script))
        # Set version
        version_file = open("{}/blobcage-files/version.txt".format(self.directory), "w")
        version_file.write(self.version)
        version_file.close()
        messages.success("Blobcage successfully installed.")

    def check_visudo(self):
        messages.info("Check /etc/sudoers for systemd-nspawn")
        visudo_output = self.term.execute_sudo_cmd("cat /etc/sudoers")
        if "ALL ALL = NOPASSWD: /usr/bin/systemd-nspawn" not in visudo_output:
            messages.warning("##############################################################\n"
                             "To make apps run without sudo password, please run (when this script stops or in other terminal) 'sudo visudo' and add line:\n"
                             "ALL ALL = NOPASSWD: /usr/bin/systemd-nspawn\n"
                             "##############################################################")
            time.sleep(15)
        else:
            messages.success("systemd-nspawn can be used without password.")

    def check_update(self):
        messages.info("Check for update.")
        ver_file_path = "{}/blobcage-files/version.txt".format(self.directory)
        if os.path.isfile(ver_file_path):
            version_file = open(ver_file_path, "r")
            current_blobcage_version = version_file.read().strip()
        else:
            current_blobcage_version = 0
        if current_blobcage_version != self.version:
            messages.info("Blobcage update needed.")
            self.update_available = True
        else:
            messages.success("Blobcage is up to date.")
            self.update_available = False

    def show_menu(self):
        update_option = "\n    7) Update Blobcage\n" if self.update_available else ""
        menu_message = """##############################################################
    Options:
    1) Steam
    2) Google Chrome
    3) Discord
    4) Battle.net (Wine)
    5) Create desktop launchers
    6) Create menu launchers{}
    X) Exit""".format(update_option)
        messages.info(menu_message)
        self.menu_option = input("Choose application to run inside BlobCage: ")

    def exe_app(self):
        messages.info("Run Blobcage.")
        if self.menu_option == "1":
            run(RUN_STEAM, shell=True)
        elif self.menu_option == "2":
            run(RUN_CHROME, shell=True)
        elif self.menu_option == "3":
            run(RUN_DISCORD, shell=True)
        elif self.menu_option == "4":
            run(RUN_BATTLE_NET_WINE, shell=True)
        elif self.menu_option == "5":
            self.prepare_launcher("steam", DESKTOP_DIR)
            self.prepare_launcher("chrome", DESKTOP_DIR)
            self.prepare_launcher("discord", DESKTOP_DIR)
            self.prepare_launcher("battlenet_wine", DESKTOP_DIR)
        elif self.menu_option == "6":
            self.prepare_launcher("steam", MENU_DIR)
            self.prepare_launcher("chrome", MENU_DIR)
            self.prepare_launcher("discord", MENU_DIR)
            self.prepare_launcher("battlenet_wine", MENU_DIR)
        elif self.menu_option == "7":
            # ToDo
            messages.info(
            "${RUN_BLOBCAGE} /blobcage-files/blobcage_update.sh && xhost - && echo $VERSION > ${DIRECTORY}blobcage-files/version.txt")
        else:
            messages.info("Exit")

    def prepare_launcher(self, name, direcory):
        messages.info("Prepare launchers.")
        icons_dir = "{}/.local/share/icons/".format(home_dir)
        if not os.path.isdir(icons_dir):
            os.makedirs(icons_dir)
        icon_path = os.path.join(blobcage_script_path, "misc", "icons", "{}.png".format(name))
        launcher_path = os.path.join(blobcage_script_path, "misc", "launchers", "{}.desktop".format(name))
        shutil.copy2(icon_path, "{}{}.png".format(icons_dir, name))
        target_laucher_lacation = os.path.join(direcory, "{}.desktop".format(name))
        shutil.copy2(launcher_path, target_laucher_lacation)
#    os.chmod(target_laucher_lacation, 777)
        launcher_exe = ""
        if name == "steam":
            launcher_exe = RUN_STEAM
        elif name == "chrome":
            launcher_exe = RUN_CHROME
        elif name == "discord":
            launcher_exe = RUN_DISCORD
        elif name == "battlenet_wine":
            launcher_exe = RUN_BATTLE_NET_WINE
        launcher_file = open(target_laucher_lacation, "r")
        launcher_list = launcher_file.readlines()
        launcher_file.close()
        launcher_string = ""
        for line in launcher_list:
            if "Exec=" in line:
                launcher_string = "{}Exec=bash -c \"{}\"\n".format(launcher_string, launcher_exe)
            else:
                launcher_string = "{}{}".format(launcher_string, line)
        launcher_file = open(target_laucher_lacation, "w")
        launcher_file.write(launcher_string)
        launcher_file.close()
        messages.success("Launchers prepared successfully.")

# ToDo: This code needs to be refactored.
AD_PARAMETERS = ""
NV_DRIVER = "NONE"
nv_dev_list = ["/dev/nvidia0", "/dev/nvidiactl", "/dev/nvidia-modeset", "/dev/video0"]
for nv_dev in nv_dev_list:
    if os.path.exists(nv_dev):
        AD_PARAMETERS = " --bind={}{}".format(nv_dev, AD_PARAMETERS)
        NV_DRIVER = r"$(glxinfo | grep OpenGL\ version | awk '{ print $NF }')"

USER_ID = os.getuid()
RUN_BLOBCAGE = 'xhost + && sudo systemd-nspawn -D {} --bind=/run/user/{}/pulse:/run/user/host/pulse --bind=/dev/snd ' \
               '--bind=/dev/dri --property=DeviceAllow="/dev/dri/renderD128" ' \
               '--setenv="DISPLAY=$DISPLAY"'.format("/srv/blobcage/", USER_ID,)
if NV_DRIVER != "NONE":
    RUN_BLOBCAGE = '{} {} --setenv=NV_DRIVER={} '.format(RUN_BLOBCAGE, AD_PARAMETERS, NV_DRIVER)
RUN_STEAM = "{} --setenv=\"CONTAINER_APP=steam\" && xhost -".format(RUN_BLOBCAGE)
RUN_CHROME = "{} --setenv=\"CONTAINER_APP=google-chrome-stable\" && xhost -".format(RUN_BLOBCAGE)
RUN_DISCORD = "{} --setenv=\"CONTAINER_APP=discord\" && xhost -".format(RUN_BLOBCAGE)
RUN_BATTLE_NET_WINE = "{} --setenv=\"CONTAINER_APP=battle_net_wine.sh\" && xhost -".format(RUN_BLOBCAGE)
home_dir = os.environ['HOME']
DESKTOP_DIR = str(check_output(['xdg-user-dir', 'DESKTOP']))[2:-3] + "/"
MENU_DIR = '{}/.local/share/applications'.format(home_dir)
blobcage_script_path = os.path.dirname(os.path.realpath(__file__)) + "/"


def run_blobcage():
    bcage = Blobcage()
    if not bcage.err_msg:
        bcage.determine_distro()
    if not bcage.err_msg:
        bcage.install_dependencies()
    if not bcage.err_msg and not os.path.isdir(bcage.directory):
        bcage.install_blobcage()
    if not bcage.err_msg:
        bcage.check_visudo()
    if not bcage.err_msg:
        bcage.check_update()
    if not bcage.err_msg:
        bcage.show_menu()
    if not bcage.err_msg:
        bcage.exe_app()
    return bcage.err_msg


if __name__ == "__main__":
    run_blobcage()
