#!/bin/bash

##This software is available to you under the terms of the GPL-3, see "/usr/share/common-licenses/GPL-3".
##Copyright:
##- Tomasz Makarewicz (tomasz.makarewicz@protonmail.ch)

rm /etc/resolv.conf
echo "nameserver 208.67.222.222" > /etc/resolv.conf
dpkg --add-architecture i386
apt update
apt -y install gnupg wget python3
echo "console-setup   console-setup/charmap47 select  UTF-8" > encoding.conf
debconf-set-selections encoding.conf
rm encoding.conf
cp /root/.bashrc /root/bash.bashrc
#Get and execute update script
mkdir /blobcage-files
touch /blobcage-files/version.txt
chmod 777 /blobcage-files/version.txt
bash /blobcage-files/blobcage_update.sh
#create user
useradd -p $(openssl passwd -1 non_secret_blobcage_password) -m blobcage
mkdir -p /home/blobcage/.steam/ubuntu12_32/steam-runtime
cp /etc/bash.bashrc /home/blobcage/.bashrc
chown -R blobcage /home/blobcage
chmod -R go+rw /home/blobcage
