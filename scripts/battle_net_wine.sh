#!/bin/bash

##This software is available to you under the terms of the GPL-3, see "/usr/share/common-licenses/GPL-3".
##Copyright:
##- Tomasz Makarewicz (tomasz.makarewicz@protonmail.ch)

if [ -e $HOME/.wine/dosdevices/c\:/Program\ Files\ \(x86\)/Battle.net/Battle.net.exe ]; then
	cd $HOME/.wine/dosdevices/c\:/Program\ Files\ \(x86\)/Battle.net/
	wine Battle.net.exe
else
	wget http://dist.blizzard.com/downloads/bna-installers/322d5bb9ae0318de3d4cde7641c96425/retail.1/Battle.net-Setup-enUS.exe
	chmod +x Battle.net-Setup-enUS.exe
	wine Battle.net-Setup-enUS.exe
	rm Battle.net-Setup-enUS.exe
fi
