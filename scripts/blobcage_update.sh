#!/bin/bash

##This software is available to you under the terms of the GPL-3, see "/usr/share/common-licenses/GPL-3".
##Copyright:
##- Tomasz Makarewicz (tomasz.makarewicz@protonmail.ch)

if [ ! -f "/blobcage-files/blobcage_update_run.sh" ]; then
	wget https://gitlab.com/makson/blobcage/raw/master/scripts/blobcage_update.sh -O /blobcage-files/blobcage_update_run.sh
	chmod 777 /blobcage-files/blobcage_update_run.sh
	bash /blobcage-files/blobcage_update_run.sh
else
	#Prepare bashrc
	if [ ! -f "/root/bash.bashrc" ]; then
		cp /root/.bashrc /root/bash.bashrc
	fi
	cp /root/bash.bashrc /root/.bashrc
	echo "mkdir /run/systemd/resolve" >> /root/.bashrc
	echo "ln -sf /etc/resolv.conf /run/systemd/resolve/resolv.conf" >> /root/.bashrc
	echo "if [ -z \$CONTAINER_APP ]" >> /root/.bashrc
	echo "then :" >> /root/.bashrc
	echo "else" >> /root/.bashrc
	echo "if [ 'steam' = \$CONTAINER_APP ] && ! dpkg -l | grep steam:i386 > /dev/null; then" >> /root/.bashrc
	echo "apt -y install steam:i386"  >> /root/.bashrc
	echo "elif [ 'discord' = \$CONTAINER_APP ] && ! dpkg -l | grep discord > /dev/null; then" >> /root/.bashrc
	echo "wget 'https://discord.com/api/download?platform=linux&format=deb' -O /blobcage-files/discord.deb"	
	echo "dpkg -i /blobcage-files/discord.deb"  >> /root/.bashrc
	echo "apt -y install -f"
	echo "elif [ 'google-chrome-stable' = \$CONTAINER_APP ] && ! dpkg -l | grep google-chrome-stable > /dev/null; then" >> /root/.bashrc
	echo "apt -y install google-chrome-stable"  >> /root/.bashrc
	echo "elif [ '/blobcage-files/battle_net_wine.sh' = \$CONTAINER_APP ] && ! dpkg -l | grep winehq-staging > /dev/null; then" >> /root/.bashrc
	echo "apt -y install winehq-staging"  >> /root/.bashrc
	echo "fi" >> /root/.bashrc
	echo "chmod 777 -R /run/user/host/pulse && apt update && apt -y upgrade && /blobcage-files/nvidia_update.sh & su blobcage -c 'cd && \$CONTAINER_APP'" >> /root/.bashrc
	echo "exit" >> /root/.bashrc
	echo "fi" >> /root/.bashrc
	#Set env variables
	echo "PULSE_SERVER=unix:/run/user/host/pulse/native" >> /etc/environment
	#Prepare repository list
	wget https://gitlab.com/makson/blobcage/raw/master/misc/sources.list -O /etc/apt/sources.list
	apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 818A435C5FCBF54A 1397BC53640DB551 1F3045A5DF7587C3 76F1A20FF987672F
	apt update
	apt -y install xterm mesa-utils mesa-vulkan-drivers va-driver-all mesa-vdpau-drivers mesa-opencl-icd vulkan-tools libva-glx2 libva-drm2 vainfo vdpauinfo beignet-opencl-icd clinfo pciutils pulseaudio
	#Detect NVIDIA system and install drivers
	if $(lspci | grep -q "VGA compatible controller: NVIDIA") && [ $(echo ${NV_DRIVER}) != $(echo 'NONE') ] ; then
		if [ $(glxinfo | grep OpenGL\ version)"a" != ${NV_DRIVER}"a" ]; then
			wget wget http://pl.download.nvidia.com/XFree86/Linux-x86_64/${NV_DRIVER}/NVIDIA-Linux-x86_64-${NV_DRIVER}.run
			chmod +x NVIDIA-Linux-*.run
			./NVIDIA-Linux-*.run --no-kernel-module --no-questions --no-install-libglvnd --ui=none
			rm ./NVIDIA-Linux-*.run
		fi
	fi
	#Prepare blobcage internal files
	wget https://gitlab.com/makson/blobcage/raw/master/scripts/battle_net_wine.sh -O /blobcage-files/battle_net_wine.sh
	wget https://gitlab.com/makson/blobcage/raw/master/scripts/nvidia_update.sh -O /blobcage-files/nvidia_update.sh
	chmod 777 /blobcage-files/battle_net_wine.sh
	chmod 777 /blobcage-files/nvidia_update.sh
	#Self file update
	rm /blobcage-files/blobcage_update.sh
	mv /blobcage-files/blobcage_update_run.sh /blobcage-files/blobcage_update.sh
fi
