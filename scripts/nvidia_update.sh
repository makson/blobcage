#!/bin/bash

##This software is available to you under the terms of the GPL-3, see "/usr/share/common-licenses/GPL-3".
##Copyright:
##- Tomasz Makarewicz (tomasz.makarewicz@protonmail.ch)

if $(lspci | grep -q "VGA compatible controller: NVIDIA") && [ "${NV_DRIVER}" != "NONE" ]; then
	if [ $(glxinfo | grep OpenGL\ version | awk '{ print $NF }')"a" != "${NV_DRIVER}""a" ]; then
		wget http://pl.download.nvidia.com/XFree86/Linux-x86_64/${NV_DRIVER}/NVIDIA-Linux-x86_64-${NV_DRIVER}.run
		chmod +x NVIDIA-Linux-*.run
		./NVIDIA-Linux-*.run --no-kernel-module --no-questions --no-install-libglvnd --ui=none
		rm ./NVIDIA-Linux-*.run
	else
		echo "NVIDIA drivers up to date"
	fi
else
	echo "No NVIDIA drivers needed"
fi
